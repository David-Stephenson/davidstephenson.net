baseURL: "https://davidstephenson.net"
title: David Stephenson
paginate: 5
theme: PaperMod

enableRobotsTXT: true
buildDrafts: false
buildFuture: false
buildExpired: false

minify:
  disableXML: true
  minifyOutput: true

params:
  env: production
  title: David Stephenson
  description: "A cool website I guess"
  keywords: [Blog, Portfolio, PaperMod]
  author: David
  # author: ["Me", "You"] # multiple authors
  images: ["<link or path of image for opengraph, twitter-cards>"]
  DateFormat: "January 2, 2006"
  defaultTheme: auto # dark, light
  disableThemeToggle: false

  ShowReadingTime: true
  ShowShareButtons: false
  ShowPostNavLinks: true
  ShowBreadCrumbs: true
  ShowCodeCopyButtons: false
  ShowWordCount: true
  ShowRssButtonInSectionTermList: true
  UseHugoToc: true
  disableSpecial1stPost: false
  disableScrollToTop: false
  comments: false
  hidemeta: false
  hideSummary: false
  showtoc: false
  tocopen: false

  assets:
    # disableHLJS: true # to disable highlight.js
    # disableFingerprinting: true
    favicon: "<link / abs url>"
    favicon16x16: "<link / abs url>"
    favicon32x32: "<link / abs url>"
    apple_touch_icon: "<link / abs url>"
    safari_pinned_tab: "<link / abs url>"

  # label:
  #   text: "David"
  #   icon: /apple-touch-icon.png
  #   iconHeight: 35

  # profile-mode
  profileMode:
    enabled: true
    title: David Stephenson
    subtitles:
      - '"Security Researcher" 🔐'
      - "Aspiring computer science student"
      - "Developer 🧑‍💻"
      - "Python -  Java - JavaScript"
      - "Eager to learn and grow in tech"
      - '<a href="https://mastodon.social/tags/FOSS">#FOSS</a> <a href="https://mastodon.social/tags/Privacy">#Privacy</a> <a href="https://mastodon.social/tags/Tech">#Tech</a>'
      - "Dedicated to making a difference through computer science"
    imageUrl: "logo.avif"
    imageWidth: 150
    imageHeight: 150
    imageTitle: logo
    buttons:
      - name: About Me
        url: about
      - name: Projects
        url: projects
      # - name: Resume
      #   url: resume

  socialIcons:
    - name: mastodon
      url: "https://mastodon.social/@DavidStephenson"
    - name: github
      url: https://github.com/David-Stephenson
    - name: gitlab
      url: "https://gitlab.com/David-Stephenson"
    - name: linkedin
      url: "https://www.linkedin.com/in/david-stephenson-977425251/"

  cover:
    hidden: true # hide everywhere but not in structured data
    hiddenInList: true # hide on list pages and home
    hiddenInSingle: true # hide on single page

  editPost:
    URL: "https://gitlab.com/David-Stephenson/davidstephenson.net/-/tree/main/content/"
    Text: "Suggest Changes" # edit text
    appendFilePath: true # to append file path to Edit link

  # for search
  # https://fusejs.io/api/options.html
  fuseOpts:
    isCaseSensitive: false
    shouldSort: true
    location: 0
    distance: 1000
    threshold: 0.4
    minMatchCharLength: 0
    keys: ["title", "permalink", "summary", "content"]
menu:
  main:
    - identifier: about
      name: About
      url: /about/
      weight: 1
    - identifier: posts
      name: Posts
      url: /posts/
      weight: 2
    - identifier: projects
      name: Projects
      url: /projects/
      weight: 3
    - identifier: cybersecurity
      name: Cyber Security
      url: /cybersecurity/
      weight: 4
    - identifier: certifications
      name: Certifications
      url: /certifications/
      weight: 5
    - identifier: contact
      name: Contact
      url: /contact/
      weight: 6
# Read: https://github.com/adityatelange/hugo-PaperMod/wiki/FAQs#using-hugos-syntax-highlighter-chroma
pygmentsUseClasses: true

markup:
  _merge: none
  highlight:
    noClasses: false
    # anchorLineNos: true
    # codeFences: true
    # guessSyntax: true
    # lineNos: true
    # style: monokai
  goldmark:
    parser:
      attribute:
        block: true
