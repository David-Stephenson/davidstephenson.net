---
title: "Certifications"
draft: false
ShowWordCount: false
ShowReadingTime: false
---

# The Linux Foundation
### [LFD107x: Open Source Software Development: Linux for Developers](https://courses.edx.org/certificates/cb732e40d7284684afddb8f747959baa)
![Problem Solving (Basic) Certificate](images/cb732e40d7284684afddb8f747959baa.avif#center)

# HackerRank

### [Problem Solving (Basic)](https://www.hackerrank.com/certificates/3a3ac0ac242a)
![Problem Solving (Basic) Certificate](images/problem-solving-basic.avif#center)

### [Python (Basic)](https://www.hackerrank.com/certificates/db07828b6dce)
![Python (Basic) Certificate](images/python-basic.avif#center)

### [JavaScript (Basic)](https://www.hackerrank.com/certificates/e4181fe65954)
![JavaScript (Basic) Certificate](images/javascript-basic.avif#center)