---
title: "Contact"
draft: false
ShowWordCount: false
ShowReadingTime: false
---

## [Session](https://getsession.org/)

ID: 051d2af3647f17bb49b4123a9d4188df2a35055435b0f17cde553af6e386057d7b

## Email

{{<email "contact@davidstephenson.net">}}

## [Matrix](https://matrix.org/)

#### I recommend the [Element client](https://element.io/)

[@davidstephenson:matrix.org](https://matrix.to/#/@davidstephenson:matrix.org)

## [Signal](https://signal.org/) I will give this out to trusted contacts by request.

> If I fail to respond on any other platform, please email me.