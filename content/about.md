---
title: "About Me"
description: ""
date: 2023-01-06T13:02:15-05:00
draft: false
ShowWordCount: false
ShowReadingTime: false
---

👋 Hello, I'm David, and I'm currently a high school student interested in studying computer science. My main areas of interest include Artificial intelligence, Cybersecurity, and Software engineering. I'm excited to learn more about these fields and develop my skills to positively impact the world. In my free time, I enjoy exploring new technologies and staying up-to-date with the latest trends in the field. I can continue to learn and grow as a developer by staying curious and keeping an open mind. I'm passionate about using my skills and knowledge to solve complex problems, and I'm determined to achieve my career goals through hard work and dedication.

### My Mission

As a computer science student, my goal is to use my passion for technology to positively impact the world. I'm excited to continue learning and growing as a developer and to use my skills to create innovative solutions. Whether working on a personal project or collaborating with others, I strive to deliver high-quality, reliable, and user-friendly products. I'm also committed to staying up-to-date with the latest trends and technologies in the field and sharing my knowledge and expertise with others.

### My Goals

1. Complete a major project

2. Pursue an internship or other real-world experience

3. Contribute to the Linux Kernel
