---
title: "Cyber Security"
draft: false
ShowWordCount: false
ShowReadingTime: false
description: "My contributions and efforts in the field of cyber security, detailing the issues I have addressed"
---

## 2023

### [MIT](https://mit.edu) - September
I discovered and reported two Log4j vulnerable Minecraft servers. 

### [CSCC Bookstore](https://bookstore.cscc.edu/) - August 
I accidentally ran into a page that exposed student information. I reported it, and it got fixed the same day.

### [Sonarr](https://sonarr.tv/) - July
I reported multiple vulnerabilities and issues that allowed remote code execution. 

### [MIT](https://mit.edu) - May
I discovered an insecure seedbox on the MIT network which was operating without any authentication mechanism. Recognizing the potential risk, I promptly reported the vulnerability to the MIT IST team.

### [OtterBox](https://www.otterbox.com/en-us/vulnerability-disclosure-policy.html) - March
I discovered a vulnerability that allowed a malicious user to send modified HTTP requests during a warranty replacement to get any amount of phone cases for free and $10 shipping.

---

## 2022

### [HDTracks](https://www.hdtracks.com/) - March
I reported a vulnerability in HDTracks that allowed users to download music for free, including unreleased tracks, without proper authorization checks. Although a HDTracks account was required, the vulnerability allowed anyone with an account to access and download the music without any restrictions or limitations.

### [Dublin City Schools](https://www.dublinschools.net/) - October
During my evaluation of my district's in-house dashboard, I discovered a vulnerability in one of its endpoints that allowed for path traversal, which allowed the unauthorized downloading of server-side code. This vulnerability led to the exposure of database credentials containing sensitive information of students, staff, and parents. Additionally, I uncovered multiple permission and XSS vulnerabilities within the same dashboard.

---

## 2021

### [Floatplane](https://floatplane.com) 
I reported an endpoint within the Floatplane API that allowed for unauthorized downloading of videos at any quality level.

### [Jellyfin Web](https://github.com/jellyfin/jellyfin-web/issues/2674)
 I found that the vulnerability stemmed from the use of innerHTML, which allowed an individual with admin panel access to exploit the message function and inject Cross-Site Scripting (XSS) into a user's web client.