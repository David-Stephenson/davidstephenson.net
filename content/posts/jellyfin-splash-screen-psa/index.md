---
title: "Jellyfin Splash Screen PSA"
description: "Potential security risks posed by the splash screen feature on Jellyfin media server and why users should consider disabling it to keep their media files private and secure."
date: 2023-03-25T06:31:47-04:00
draft: false
tags: ["Security Research"]
---

[Jellyfin](https://jellyfin.org/) is an open-source media server that allows users to store, organize, and stream their media files. While it offers various features to enhance the user's media experience, there is one feature that users should be aware of: the splash screen.

The splash screen displays a small collection of the media library before the user logs in. Although it may seem like a nice touch to add to your server, it can pose a potential security risk.

## How to Access the Splash Screen

If the splash screen feature is enabled on a Jellyfin server, anyone with the server URL can access the splash screen image by adding `/Branding/Splashscreen` to the end of the server URL.

## The Risk of the Splash Screen

When the splash screen is enabled, it displays a small selection of media files to give users a preview of the content available on the server. While this feature can be helpful for new users, it can also be problematic for users who want to keep their media files private.

The splash screen can display images of movies, TV shows, and music files stored on the server. This information can be used by anyone who has access to the server to gain insight into the user's media library.

This information might seem harmless, but it can reveal more than you think. For example, if a user has a collection of sensitive or embarrassing media files, such as adult movies or personal videos, they can be easily identified by someone who sees the splash screen. This could lead to unwanted and potentially embarrassing situations.

## The Risk of Publicly Accessible Servers

If a user has made their Jellyfin server publicly accessible, anyone with the server's URL can access it and potentially view the splash screen. This can be a privacy concern if users have sensitive media files stored on their server.

I came across a Jellyfin server being used to store family photos. However, due to the enabled splash screen, some of these personal photos were being displayed alongside normal movie posters, potentially leading to unintentional sharing of private images.

![Censored example](images/family.avif#center)

## How to Disable the Splash Screen

Fortunately, Jellyfin allows users to disable the splash screen feature. To disable the splash screen on the Jellyfin server, users can follow these steps:

1. Log in to the Jellyfin server.
2. Go to the admin dashboard.
3. Click on the "General" tab.
4. Toggle the "Enable the splash screen" option to "Off."
5. Click "Save" to apply the changes.

## Examples

Here are a five splash screens I found on publicy exposed Jellyfin servers...

![Example 1](images/1.avif#center)
![Example 2](images/2.avif#center)
![Example 3](images/3.avif#center)
![Example 4](images/4.avif#center)
![Example 5](images/5.avif#center)