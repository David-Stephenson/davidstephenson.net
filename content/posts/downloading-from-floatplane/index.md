---
title: "Downloading From Floatplane"
description: "Learn about the vulnerability that allowed you to download videos from Floatplane for free."
date: 2022-11-24
draft: false
tags: ["Security Research"]
---

> This vulnerability has been patched an no longer works!

You might be asking yourself, what is Floatplane? Floatplane is a streaming service for “creators to upload their videos in high quality while earning money from their supporters directly” (Linus Tech Tips 0:43). With that said, Floatplane has an vulnerability that allows anyone with a Floatplane account to download a creator's video content for free without paying the creator. This vulnerability is an issue because it enables people to steal from small studios and independent creators who have people to feed and do not make billions of dollars like Disney, Sony, Viacom, etc.

> A Floatplane account isn't required to perform this vulnerability. However, it is recommended because it makes the process a lot easier.

## Discovering the Vulnerability

I used to be subscribed to LinusTechTips on Floatplane, so I had access to more features on Floatplane than an account without a subscription. Access to these features helped me discover this vulnerability.
![My Floatplane invoice](images/invoice.avif#center)

You know that Floatplane subscribers have access to streaming videos, but did you know that they have access to downloading videos? When downloading videos, the download URL for the video is located on a Floatplane edge node. The URL contains an attachment ID and the video's resolution. When downloading from the edge node, no authentication is required.

![Example of Floatplane edge URL](images/edge-url.avif#center)

Accounts without an active subscription won't usually have access to the download URL template. All it takes is one person to leak it and for someone else to understand how to abuse it.

![Message of leaked download URL](images/leak.avif#center)

The attachment ID is only for downloading videos from a Floatplane post; the ID has nothing to do with streaming video.

One day I was browsing through Floatplane with my browser's Network inspection panel open, and I discovered a JSON key called attachmentOrder in one of the requests to the Floatplane API. The attachmentOrder key contains attachment IDs that can be used with the download URL. I tried the IDs with the download URL, and low and behold, the videos started downloading. I tried going to a creator's channel for which I didn't have a subscription, and the attachment keys were still showing up in the request. I was confused why the IDs were there if I did not have an active subscription, but I tried the IDs in the download URL anyway, and the videos started downloading.

I am still confused about why the attachment IDs are supplied to users without an active subscription. I'm so confused about why the attachment ID was there in the first place. The web request was made while browsing on the content browser; you can't directly download from that page.

## Reproducing the Vulnerability

First, go to [floatplane.com/browse](https://floatplane.com/browse) and open your browser's network inspection tools. Back of Floatplane, click on the creator you're interested in downloading a video from. Filter the requests by requests that contain the text "/api/v3/content/creator". You should see at least one network request.

![Network Requests](images/requests.avif#center)

When you inspect the network request, you will see a JSON array containing JSON objects. Each one of the objects will include information about a creator's posts, the post's title, description, thumbnail, metadata, and most importantly, the attachment IDs.

![Example array](images/array.avif#center)

Copy the attachment ID inside attachmentOrder and add it to the download URL "https://edge01-na.floatplane.com/Videos/attachmentID". If attachmentOrder contains more than one attachment ID, you should try each ID separately until you find what you're looking for.

You now need to specify what resolution the video should download in. Unfortunately, there's no way to tell what the max resolution of a video is, but Floatplane currently supports up to 2160p, making the guesswork a little bit easier. Some of the common resolutions you can try are 2160p, 1080p, 720p, 480p, and 360p. Add the resolution you want onto the end of the download URL without the 'p' and add '.mp4' to the end "https://edge01-na.floatplane.com/Videos/attachmentID/1080.mp4". If a resolution for a video doesn't exist, the server will return with "Not found".

The video should start downloading or playing in your browser if you did everything correctly.

![Example video](images/video.avif#center)

{{< video "videos/demo.mp4" "my-5" >}}
