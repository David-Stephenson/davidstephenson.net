---
title: "Lego Mass Launcher"
description: ""
date: 2023-08-28T07:52:46-04:00
draft: False
---

On the first day of Engineering Academy, we started our first project of the year: the Lego Mass Launcher. The mass launcher needed to launch a plastic ball using only Lego Technic and Lego Mindstorm EV3 pieces. Once the ball was in motion, you could no longer touch your build. There could be no user assistance in moving the ball. The class was split into groups of three, and I was paired with Ashrey and Liam. I don't think I'm alone when I say this, but this first project was challenging because it was my first time using these types of Lego pieces.

We discussed what we could build as a group and made basic prototypes to understand the various Lego components better. Our first idea was to use two motors and wheels to launch the ball, but unfortunately, the motors were too slow to make the ball move a large distance. We then tried to build a ramp that would launch the ball using potential and kinetic energy, but we soon scraped that idea when we realized we wouldn't have enough pieces to complete the ramp. After further discussion, we decided that we would build a catapult.

![Liams Sketch](images/sketch.avif)
A simple skstch drawn by Liam of our first idea.
{.center}

While discussing the design for the catapult, we decided that we needed to build three parts: the bucket, arm, and base. The bucket needed to hold the ball without gripping onto the ball so that way the ball would launch when the user stopped pulling instead of staying connected to the bucket. The arm needed to build up elastic potential energy and join the bucket to the base. The base needed to support the weight of all the pieces and the force of the user pulling back on the bucket.

### Challenges

During this project we hit a few roadblocks. First, we discovered that our base was too light even to support the weight of the bucket and arm. We fixed this by adding more pieces to the base to weigh it down, fixing the issue.

When we tested the catapult and applied force to the head, we discovered that the base was still too light and the catapult would tip over. We tried adding more pieces to the base to fix this issue, but this didn't work because we were running out of pieces, and it was becoming messy and unbalanced. We needed to rethink our base quickly because we were running out of time before we had to test. I discovered that the motors and giant wheels inside the Mindstorm kit were pretty heavy, and I proposed that we build a base almost like a car. I assembled the base and showed it to my group members. They liked the idea and attached the catapult to the new base. This new base was the solution we were looking for because it allowed the user to apply a large enough force to build up the elastic potential energy to launch the ball but not enough to cause the catapult to fall over.

A few photos of our final build:
![Photo 1](images/PXL_20230818_162546961.avif)
![Photo 2, courtesy of Ashrey](images/image_67200257.avif)
![Photo 3](images/PXL_20230818_162544116.avif)

> Ignore the wires, I'm not sure why they are plugged in. 😅

### Testing

We collected the distance the ball traveled during testing and how long it took. Ashrey was responsible for recording our tests in slow motion, Liam was responsible for launching the ball, and I was responsible for collecting the time. We tested and collected data four times. The data then would be used to calculate the velocity of the ball.

| Time (seconds) | Distance (Centimeters) | Velocity (m/s) |
| -------------- | ---------------------- | -------------- |
| 0.8            | 48.26                  | 0.6657         |

{{< video "videos/video.mp4" "my-5" >}}

As seen in the video, the ball didn't launch very far. It's a bit disappointing, but with further modifications to the arm, such as height, we could probably get the bar to launch further.

### What I learned

I learned a lot of new things while working on this project, such as how to use Lego Technic and Lego Mindstorm EV3 pieces and the importance of heavy base. This project was also a refresher on calculating velocity since I last used the equation in AP Physics.
