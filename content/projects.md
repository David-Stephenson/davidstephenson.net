---
title: "Projects"
draft: false
ShowWordCount: false
ShowReadingTime: false
---

### [Manga Plus Downloader](https://gitlab.com/David-Stephenson/manga-plus-downloader)
This project is simple Python CLI command that allows you to backup manga from [Manga Plus](https://mangaplus.shueisha.co.jp/updates).

---

### [Gradients](https://gitlab.com/David-Stephenson/gradients)
Quickly generate color stops between two colors.

---

### [PreDB 2000](https://gitlab.com/David-Stephenson/predb-2000)

PreDB 2000, inspired by [Shodan 2000](https://2000.shodan.io), displays the latest scene releases in a synthwave style that's easy on the eyes.

I built this project using [SvelteKit](https://kit.svelte.dev/), [TailwindCSS](https://tailwindcss.com/), [ThreeJS](https://threejs.org/), and [PreDB](https://predb.net/).

[Demo](https://predb-2000.pages.dev/)

---

### [Quotogram](https://gitlab.com/David-Stephenson/quotogram)

Quotogram is a Python script that produces inspirational quote images by merging stunning backgrounds from Pexels and Unsplash with meaningful quotes from API Ninjas. The generated pictures are perfect for sharing on various social media platforms, including Instagram and Facebook.

---

### [Compress For My Sake](https://gitlab.com/David-Stephenson/compress-for-my-sake)

This tool is designed to compress manga images and reduce their file size while maintaining high quality. It achieves this by using the AVIF image format, which provides superior compression efficiency compared to other formats like JPEG and PNG.

---